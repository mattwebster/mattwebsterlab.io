$ obj3
                     \|||||||||||||||||/
                   --- Welcome to OBJ3 ---
                     /|||||||||||||||||\
         OBJ3 version 2.0  built: 2001 Aug 10 Fri 19:17:11
            Copyright 1988,1989,1991 SRI International
                   2006 Jul 26 Wed 16:12:49
OBJ> in sta

==========================================
obj BITWISE
==========================================
obj IA-32-SYNTAX
==========================================
obj IA-32-SEMANTICS
OBJ> in proof

==========================================
open
==========================================
ops a b : Store -> Store .
==========================================
eq a ( S ) = S ; push ebp ; mov ebp, esp .
==========================================
eq b ( S ) = S ; push ebp ; push esp ; pop ebp .
==========================================
reduce in IA-32-SEMANTICS : a(s)[[ip]] is b(s)[[ip]]
rewrites: 21
result Bool: false
==========================================
reduce in IA-32-SEMANTICS : a(s)[[stack]] is b(s)[[stack]]
rewrites: 15
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : a(s)[[ebp]] is b(s)[[ebp]]
rewrites: 21
result Bool: true
==========================================
close
==========================================
open
==========================================
op dword1 : -> EInt .
==========================================
ops c d : Store -> Store .
==========================================
eq c ( S ) = S ; mov esi, dword1 ; test esi, esi .
==========================================
eq d ( S ) = S ; mov esi, dword1 ; or esi, esi .
==========================================
eq I bor I = I .
==========================================
eq I band I = I .
==========================================
reduce in IA-32-SEMANTICS : c(s)[[esi]] is d(s)[[esi]]
rewrites: 41
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : c(s)[[ip]] is d(s)[[ip]]
rewrites: 40
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : c(s)[[zf]] is d(s)[[zf]]
rewrites: 42
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : c(s)[[pf]] is d(s)[[pf]]
rewrites: 42
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : c(s)[[sf]] is d(s)[[sf]]
rewrites: 42
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : c(s)[[cf]] is d(s)[[cf]]
rewrites: 30
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : c(s)[[of]] is d(s)[[of]]
rewrites: 30
result Bool: true
==========================================
close
==========================================
open
==========================================
op dword2 : -> EInt .
==========================================
ops e f : Store -> Store .
==========================================
eq e ( S ) = S ; mov edi, dword2 ; or edi, edi .
==========================================
eq f ( S ) = S ; mov edi, dword2 ; test edi, edi .
==========================================
eq I bor I = I .
==========================================
eq I band I = I .
==========================================
reduce in IA-32-SEMANTICS : e(s)[[edi]] is f(s)[[edi]]
rewrites: 41
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : e(s)[[ip]] is f(s)[[ip]]
rewrites: 40
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : e(s)[[zf]] is f(s)[[zf]]
rewrites: 42
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : e(s)[[pf]] is f(s)[[pf]]
rewrites: 42
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : e(s)[[sf]] is f(s)[[sf]]
rewrites: 42
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : e(s)[[cf]] is f(s)[[cf]]
rewrites: 30
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : e(s)[[of]] is f(s)[[of]]
rewrites: 30
result Bool: true
==========================================
close
OBJ> in proof2

==========================================
open
==========================================
ops g h : Store -> Store .
==========================================
eq g ( S ) = S ; mov edi, 2580774443 ; mov ebx, 467750807 ; sub ebx
    , 1745609157 ; sub edi, 150468176 ; xor ebx, 875205167 ; push edi
    ; xor edi, 3761393434 ; push ebx ; push edi .
==========================================
eq h ( S ) = S ; mov ebx, 535699961 ; mov edx, 1490897411 ; xor ebx
    , 2402657826 ; mov ecx, 3802877865 ; xor edx, 3743593982 ; add
    ecx, 2386458904 ; push ebx ; push edx ; push ecx .
==========================================
reduce in IA-32-SEMANTICS : g(s)[[stack]] is h(s)[[stack]]
rewrites: 292
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : g(s)[[stack]]
rewrites: 155
result Stack: 1894369473 next (2281701373 next (2430306267 next (s[[stack]])))
==========================================
close
OBJ> in lemma1

==========================================
open
==========================================
ops s1 s2 : -> Store .
==========================================
op v1 : -> Variable .
==========================================
op i1 : -> Int .
==========================================
eq s1 [ [ ip ] ] = s2 [ [ ip ] ] .
==========================================
reduce in IA-32-SEMANTICS : s1 ; mov v1,i1[[v1]] is s2 ; mov v1,i1[[
    v1]]
rewrites: 12
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; mov v1,i1[[ip]] is s2 ; mov v1,i1[[
    ip]]
rewrites: 11
result Bool: true
==========================================
close
==========================================
open
==========================================
ops s1 s2 : -> Store .
==========================================
ops v1 v2 : -> Variable .
==========================================
eq s1 [ [ v2 ] ] = s2 [ [ v2 ] ] .
==========================================
eq s1 [ [ ip ] ] = s2 [ [ ip ] ] .
==========================================
reduce in IA-32-SEMANTICS : s1 ; mov v1,v2[[v1]] is s2 ; mov v1,v2[[
    v1]]
rewrites: 11
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; mov v1,v2[[ip]] is s2 ; mov v1,v2[[
    ip]]
rewrites: 11
result Bool: true
==========================================
close
==========================================
open
==========================================
ops s1 s2 : -> Store .
==========================================
op v1 : -> Variable .
==========================================
op i1 : -> Int .
==========================================
eq s1 [ [ ip ] ] = s2 [ [ ip ] ] .
==========================================
eq s1 [ [ v1 ] ] = s2 [ [ v1 ] ] .
==========================================
reduce in IA-32-SEMANTICS : s1 ; add v1,i1[[v1]] is s2 ; add v1,i1[[
    v1]]
rewrites: 13
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; add v1,i1[[ip]] is s2 ; add v1,i1[[
    ip]]
rewrites: 11
result Bool: true
==========================================
close
==========================================
open
==========================================
ops s1 s2 : -> Store .
==========================================
ops v1 v2 : -> Variable .
==========================================
eq s1 [ [ v2 ] ] = s2 [ [ v2 ] ] .
==========================================
eq s1 [ [ ip ] ] = s2 [ [ ip ] ] .
==========================================
eq s1 [ [ v1 ] ] = s2 [ [ v1 ] ] .
==========================================
reduce in IA-32-SEMANTICS : s1 ; add v1,v2[[v1]] is s2 ; add v1,v2[[
    v1]]
rewrites: 12
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; add v1,v2[[ip]] is s2 ; add v1,v2[[
    ip]]
rewrites: 11
result Bool: true
==========================================
close
==========================================
open
==========================================
ops s1 s2 : -> Store .
==========================================
op v1 : -> Variable .
==========================================
op i1 : -> Int .
==========================================
eq s1 [ [ ip ] ] = s2 [ [ ip ] ] .
==========================================
eq s1 [ [ v1 ] ] = s2 [ [ v1 ] ] .
==========================================
reduce in IA-32-SEMANTICS : s1 ; sub v1,i1[[v1]] is s2 ; sub v1,i1[[
    v1]]
rewrites: 15
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; sub v1,i1[[ip]] is s2 ; sub v1,i1[[
    ip]]
rewrites: 11
result Bool: true
==========================================
close
==========================================
open
==========================================
ops s1 s2 : -> Store .
==========================================
ops v1 v2 : -> Variable .
==========================================
eq s1 [ [ v2 ] ] = s2 [ [ v2 ] ] .
==========================================
eq s1 [ [ ip ] ] = s2 [ [ ip ] ] .
==========================================
eq s1 [ [ v1 ] ] = s2 [ [ v1 ] ] .
==========================================
reduce in IA-32-SEMANTICS : s1 ; sub v1,v2[[v1]] is s2 ; sub v1,v2[[
    v1]]
rewrites: 14
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; sub v1,v2[[ip]] is s2 ; sub v1,v2[[
    ip]]
rewrites: 11
result Bool: true
==========================================
close
==========================================
open
==========================================
ops s1 s2 : -> Store .
==========================================
op i1 : -> Int .
==========================================
eq s1 [ [ ip ] ] = s2 [ [ ip ] ] .
==========================================
eq s1 [ [ stack ] ] = s2 [ [ stack ] ] .
==========================================
reduce in IA-32-SEMANTICS : s1 ; push i1[[stack]] is s2 ; push i1[[stack]]
rewrites: 9
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; push i1[[ip]] is s2 ; push i1[[ip]]
rewrites: 7
result Bool: true
==========================================
close
==========================================
open
==========================================
ops s1 s2 : -> Store .
==========================================
op v1 : -> Variable .
==========================================
eq s1 [ [ ip ] ] = s2 [ [ ip ] ] .
==========================================
eq s1 [ [ stack ] ] = s2 [ [ stack ] ] .
==========================================
eq s1 [ [ v1 ] ] = s2 [ [ v1 ] ] .
==========================================
reduce in IA-32-SEMANTICS : s1 ; push v1[[stack]] is s2 ; push v1[[stack]]
rewrites: 8
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; push v1[[ip]] is s2 ; push v1[[ip]]
rewrites: 7
result Bool: true
==========================================
close
==========================================
open
==========================================
ops s1 s2 : -> Store .
==========================================
op v1 : -> Variable .
==========================================
eq s1 [ [ ip ] ] = s2 [ [ ip ] ] .
==========================================
eq s1 [ [ stack ] ] = s2 [ [ stack ] ] .
==========================================
reduce in IA-32-SEMANTICS : s1 ; pop v1[[stack]] is s2 ; pop v1[[stack]]
rewrites: 5
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; pop v1[[ip]] is s2 ; pop v1[[ip]]
rewrites: 11
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; pop v1[[v1]] is s2 ; pop v1[[v1]]
rewrites: 11
result Bool: true
==========================================
close
==========================================
open
==========================================
ops s1 s2 : -> Store .
==========================================
op v1 : -> Variable .
==========================================
op i1 : -> Int .
==========================================
eq s1 [ [ ip ] ] = s2 [ [ ip ] ] .
==========================================
eq s1 [ [ v1 ] ] = s2 [ [ v1 ] ] .
==========================================
reduce in IA-32-SEMANTICS : s1 ; and v1,i1[[v1]] is s2 ; and v1,i1[[
    v1]]
rewrites: 33
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; and v1,i1[[ip]] is s2 ; and v1,i1[[
    ip]]
rewrites: 31
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; and v1,i1[[cf]] is s2 ; and v1,i1[[
    cf]]
rewrites: 30
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; and v1,i1[[zf]] is s2 ; and v1,i1[[
    zf]]
rewrites: 33
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; and v1,i1[[of]] is s2 ; and v1,i1[[
    of]]
rewrites: 30
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; and v1,i1[[pf]] is s2 ; and v1,i1[[
    pf]]
rewrites: 33
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; and v1,i1[[sf]] is s2 ; and v1,i1[[
    sf]]
rewrites: 33
result Bool: true
==========================================
close
==========================================
open
==========================================
ops s1 s2 : -> Store .
==========================================
ops v1 v2 : -> Variable .
==========================================
eq s1 [ [ v2 ] ] = s2 [ [ v2 ] ] .
==========================================
eq s1 [ [ ip ] ] = s2 [ [ ip ] ] .
==========================================
eq s1 [ [ v1 ] ] = s2 [ [ v1 ] ] .
==========================================
reduce in IA-32-SEMANTICS : s1 ; and v1,v2[[v1]] is s2 ; and v1,v2[[
    v1]]
rewrites: 32
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; and v1,v2[[ip]] is s2 ; and v1,v2[[
    ip]]
rewrites: 31
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; and v1,v2[[cf]] is s2 ; and v1,v2[[
    cf]]
rewrites: 30
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; and v1,v2[[zf]] is s2 ; and v1,v2[[
    zf]]
rewrites: 32
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; and v1,v2[[of]] is s2 ; and v1,v2[[
    of]]
rewrites: 30
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; and v1,v2[[pf]] is s2 ; and v1,v2[[
    pf]]
rewrites: 32
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; and v1,v2[[sf]] is s2 ; and v1,v2[[
    sf]]
rewrites: 32
result Bool: true
==========================================
close
==========================================
open
==========================================
ops s1 s2 : -> Store .
==========================================
op v1 : -> Variable .
==========================================
op i1 : -> Int .
==========================================
eq s1 [ [ ip ] ] = s2 [ [ ip ] ] .
==========================================
eq s1 [ [ v1 ] ] = s2 [ [ v1 ] ] .
==========================================
reduce in IA-32-SEMANTICS : s1 ; or v1,i1[[v1]] is s2 ; or v1,i1[[v1]]
rewrites: 33
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; or v1,i1[[ip]] is s2 ; or v1,i1[[ip]]
rewrites: 31
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; or v1,i1[[cf]] is s2 ; or v1,i1[[cf]]
rewrites: 30
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; or v1,i1[[zf]] is s2 ; or v1,i1[[zf]]
rewrites: 33
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; or v1,i1[[of]] is s2 ; or v1,i1[[of]]
rewrites: 30
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; or v1,i1[[pf]] is s2 ; or v1,i1[[pf]]
rewrites: 33
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; or v1,i1[[sf]] is s2 ; or v1,i1[[sf]]
rewrites: 33
result Bool: true
==========================================
close
==========================================
open
==========================================
ops s1 s2 : -> Store .
==========================================
ops v1 v2 : -> Variable .
==========================================
eq s1 [ [ v1 ] ] = s2 [ [ v1 ] ] .
==========================================
eq s1 [ [ v2 ] ] = s2 [ [ v2 ] ] .
==========================================
eq s1 [ [ ip ] ] = s2 [ [ ip ] ] .
==========================================
reduce in IA-32-SEMANTICS : s1 ; or v1,v2[[v1]] is s2 ; or v1,v2[[v1]]
rewrites: 32
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; or v1,v2[[ip]] is s2 ; or v1,v2[[ip]]
rewrites: 31
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; or v1,v2[[cf]] is s2 ; or v1,v2[[cf]]
rewrites: 30
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; or v1,v2[[zf]] is s2 ; or v1,v2[[zf]]
rewrites: 32
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; or v1,v2[[of]] is s2 ; or v1,v2[[of]]
rewrites: 30
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; or v1,v2[[pf]] is s2 ; or v1,v2[[pf]]
rewrites: 32
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; or v1,v2[[sf]] is s2 ; or v1,v2[[sf]]
rewrites: 32
result Bool: true
==========================================
close
==========================================
open
==========================================
ops s1 s2 : -> Store .
==========================================
op v1 : -> Variable .
==========================================
op i1 : -> Int .
==========================================
eq s1 [ [ v1 ] ] = s2 [ [ v1 ] ] .
==========================================
eq s1 [ [ ip ] ] = s2 [ [ ip ] ] .
==========================================
reduce in IA-32-SEMANTICS : s1 ; xor v1,i1[[v1]] is s2 ; xor v1,i1[[
    v1]]
rewrites: 33
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; xor v1,i1[[ip]] is s2 ; xor v1,i1[[
    ip]]
rewrites: 31
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; xor v1,i1[[cf]] is s2 ; xor v1,i1[[
    cf]]
rewrites: 30
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; xor v1,i1[[zf]] is s2 ; xor v1,i1[[
    zf]]
rewrites: 33
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; xor v1,i1[[of]] is s2 ; xor v1,i1[[
    of]]
rewrites: 30
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; xor v1,i1[[pf]] is s2 ; xor v1,i1[[
    pf]]
rewrites: 33
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; xor v1,i1[[sf]] is s2 ; xor v1,i1[[
    sf]]
rewrites: 33
result Bool: true
==========================================
close
==========================================
open
==========================================
ops s1 s2 : -> Store .
==========================================
ops v1 v2 : -> Variable .
==========================================
eq s1 [ [ v1 ] ] = s2 [ [ v1 ] ] .
==========================================
eq s1 [ [ v2 ] ] = s2 [ [ v2 ] ] .
==========================================
eq s1 [ [ ip ] ] = s2 [ [ ip ] ] .
==========================================
reduce in IA-32-SEMANTICS : s1 ; xor v1,v2[[v1]] is s2 ; xor v1,v2[[
    v1]]
rewrites: 32
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; xor v1,v2[[ip]] is s2 ; xor v1,v2[[
    ip]]
rewrites: 31
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; xor v1,v2[[cf]] is s2 ; xor v1,v2[[
    cf]]
rewrites: 30
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; xor v1,v2[[zf]] is s2 ; xor v1,v2[[
    zf]]
rewrites: 32
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; xor v1,v2[[of]] is s2 ; xor v1,v2[[
    of]]
rewrites: 30
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; xor v1,v2[[pf]] is s2 ; xor v1,v2[[
    pf]]
rewrites: 32
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; xor v1,v2[[sf]] is s2 ; xor v1,v2[[
    sf]]
rewrites: 32
result Bool: true
==========================================
close
==========================================
open
==========================================
ops s1 s2 : -> Store .
==========================================
op v1 : -> Variable .
==========================================
op i1 : -> Int .
==========================================
eq s1 [ [ ip ] ] = s2 [ [ ip ] ] .
==========================================
eq s1 [ [ v1 ] ] = s2 [ [ v1 ] ] .
==========================================
reduce in IA-32-SEMANTICS : s1 ; test v1,i1[[ip]] is s2 ; test v1,i1[[
    ip]]
rewrites: 27
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; test v1,i1[[cf]] is s2 ; test v1,i1[[
    cf]]
rewrites: 26
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; test v1,i1[[zf]] is s2 ; test v1,i1[[
    zf]]
rewrites: 29
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; test v1,i1[[of]] is s2 ; test v1,i1[[
    of]]
rewrites: 26
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; test v1,i1[[pf]] is s2 ; test v1,i1[[
    pf]]
rewrites: 29
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; test v1,i1[[sf]] is s2 ; test v1,i1[[
    sf]]
rewrites: 29
result Bool: true
==========================================
close
==========================================
open
==========================================
ops s1 s2 : -> Store .
==========================================
ops v1 v2 : -> Variable .
==========================================
eq s1 [ [ v1 ] ] = s2 [ [ v1 ] ] .
==========================================
eq s1 [ [ v2 ] ] = s2 [ [ v2 ] ] .
==========================================
eq s1 [ [ ip ] ] = s2 [ [ ip ] ] .
==========================================
reduce in IA-32-SEMANTICS : s1 ; test v1,v2[[ip]] is s2 ; test v1,v2[[
    ip]]
rewrites: 27
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; test v1,v2[[cf]] is s2 ; test v1,v2[[
    cf]]
rewrites: 26
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; test v1,v2[[zf]] is s2 ; test v1,v2[[
    zf]]
rewrites: 28
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; test v1,v2[[of]] is s2 ; test v1,v2[[
    of]]
rewrites: 26
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; test v1,v2[[pf]] is s2 ; test v1,v2[[
    pf]]
rewrites: 28
result Bool: true
==========================================
reduce in IA-32-SEMANTICS : s1 ; test v1,v2[[sf]] is s2 ; test v1,v2[[
    sf]]
rewrites: 28
result Bool: true
==========================================
close
==========================================
open
==========================================
ops s1 s2 : -> Store .
==========================================
eq s1 [ [ ip ] ] = s2 [ [ ip ] ] .
==========================================
reduce in IA-32-SEMANTICS : s1 ; nop[[ip]] is s2 ; nop[[ip]]
rewrites: 7
result Bool: true
==========================================
close
OBJ> q
$
